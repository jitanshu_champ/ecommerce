package com.champ.ecommerce.ui.home

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.champ.ecommerce.R
import com.champ.ecommerce.network.beans.ProductCatalogue


class HomeFragment : Fragment() {

    private lateinit var homeViewModel: HomeViewModel

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        homeViewModel =
                ViewModelProvider(this).get(HomeViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_home, container, false)
        val progressBar: ProgressBar = root.findViewById(R.id.progress_bar)
        val infoText: TextView = root.findViewById(R.id.infoText)
        val infoTextSuccess: TextView = root.findViewById(R.id.infoTextSuccess)
        progressBar.visibility = View.VISIBLE;
        infoText.visibility = View.VISIBLE
        infoTextSuccess.visibility = View.GONE

        homeViewModel.getData()?.observe(viewLifecycleOwner,
            Observer<ProductCatalogue?> { products ->
                if (products != null) {
                    if(products.categories.isNotEmpty()){
                        progressBar.visibility = View.GONE
                        infoText.visibility = View.GONE
                        infoTextSuccess.visibility = View.VISIBLE
                    }
                }
                Log.e("TestFragment Data",products?.rankings?.size.toString())
            })

        return root
    }
}

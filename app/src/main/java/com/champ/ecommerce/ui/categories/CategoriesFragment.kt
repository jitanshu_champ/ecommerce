package com.champ.ecommerce.ui.categories

import android.content.res.Configuration
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.champ.ecommerce.R
import com.champ.ecommerce.common.CategoriesAdapter
import com.champ.ecommerce.network.beans.Categories


class CategoriesFragment : Fragment() {

    private lateinit var categoriesViewModel: CategoriesViewModel
    private lateinit var categoriesAdapter: CategoriesAdapter
    private lateinit var recyclerView: RecyclerView


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        categoriesViewModel =
            ViewModelProvider(this).get(CategoriesViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_categories, container, false)
        recyclerView = root.findViewById(R.id.rv_categories)
        prepareRecyclerView(emptyList())
        categoriesViewModel.getData()?.observe(viewLifecycleOwner,
            Observer<List<Categories>?> {
                prepareRecyclerView(it)
        })
        return root
    }

    private fun prepareRecyclerView(categoryList: List<Categories>?) {
        categoriesAdapter =
            CategoriesAdapter(activity, categoryList)
        if (this.resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT) {
            recyclerView.layoutManager = LinearLayoutManager(activity)
        } else {
            recyclerView.layoutManager = GridLayoutManager(activity, 4)
        }
        recyclerView.itemAnimator = DefaultItemAnimator()
        recyclerView.adapter = categoriesAdapter
    }

}

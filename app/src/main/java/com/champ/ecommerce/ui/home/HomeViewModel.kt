package com.champ.ecommerce.ui.home

import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.champ.ecommerce.network.Api
import com.champ.ecommerce.network.beans.ProductCatalogue
import com.champ.ecommerce.utils.BASE_URL
import com.champ.ecommerce.utils.ModelPreferencesManager
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


class HomeViewModel : ViewModel(){

    private var productCatalogue: MutableLiveData<ProductCatalogue>? = null


    fun getData(): MutableLiveData<ProductCatalogue>? {

        if (productCatalogue == null) {
            productCatalogue = MutableLiveData()
            getProducts()
        }
        return productCatalogue
    }

    private fun getProducts() {
        val retrofit: Retrofit = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        val api = retrofit.create(Api::class.java)
        val call: Call<ProductCatalogue>? = api.getProducts()
        call?.enqueue(object : Callback<ProductCatalogue?> {
            override fun onResponse(
                call: Call<ProductCatalogue?>?,
                response: Response<ProductCatalogue?>) {
                response.body()?.let {
                    productCatalogue?.value = it
                    val data = Gson()
                    val productData = data.toJson(it)
                    cacheData(productData)
                }
            }

            override fun onFailure(
                call: Call<ProductCatalogue?>?,
                t: Throwable?
            ) {
            }
        })
    }

    fun cacheData(productData: String){
        ModelPreferencesManager.put(productData, "productData")
    }
}
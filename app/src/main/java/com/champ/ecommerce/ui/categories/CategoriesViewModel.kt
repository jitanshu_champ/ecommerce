package com.champ.ecommerce.ui.categories

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.champ.ecommerce.network.beans.Categories
import com.champ.ecommerce.network.beans.ProductCatalogue
import com.champ.ecommerce.utils.ModelPreferencesManager
import com.google.gson.Gson


class CategoriesViewModel : ViewModel() {
    private lateinit var productCatalogue: ProductCatalogue
    private var categories: MutableLiveData<List<Categories>>? = null

    fun getData(): MutableLiveData<List<Categories>>? {

        if (categories == null) {
            categories = MutableLiveData()
            getProducts()
        }
        return categories
    }

    fun getProducts(): MutableLiveData<List<Categories>>? {
        val categoriesPref = ModelPreferencesManager.get<String>("productData")
        categoriesPref?.let {
                data ->
            val gson = Gson()
            productCatalogue = (gson.fromJson(data, ProductCatalogue::class.java))
            if (productCatalogue.categories.isNotEmpty())
                categories?.value = productCatalogue.categories
            return categories
        }
        return categories
    }
}
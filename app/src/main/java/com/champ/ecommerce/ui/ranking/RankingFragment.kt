package com.champ.ecommerce.ui.ranking

import android.content.res.Configuration
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.champ.ecommerce.R
import com.champ.ecommerce.common.RankingAdapter
import com.champ.ecommerce.network.beans.Rankings

class RankingFragment : Fragment() {

    private lateinit var rankingAdapter: RankingAdapter

    private lateinit var rankingViewModel: RankingViewModel
    private lateinit var recyclerView: RecyclerView

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        rankingViewModel =
            ViewModelProvider(this).get(RankingViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_ranking, container, false)
        recyclerView = root.findViewById(R.id.rv_rankings)
        rankingViewModel.getData()?.observe(viewLifecycleOwner,
            Observer<List<Rankings>?> {
                prepareRecyclerView(it)
            })
        return root
    }

    private fun prepareRecyclerView(list: List<Rankings>?) {
        rankingAdapter =
            RankingAdapter(activity, list)
        if (this.resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT) {
            recyclerView.layoutManager = LinearLayoutManager(activity)
        } else {
            recyclerView.layoutManager = GridLayoutManager(activity, 4)
        }
        recyclerView.itemAnimator = DefaultItemAnimator()
        recyclerView.adapter = rankingAdapter
    }
}

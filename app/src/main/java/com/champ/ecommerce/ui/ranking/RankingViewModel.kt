package com.champ.ecommerce.ui.ranking

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.champ.ecommerce.network.beans.ProductCatalogue
import com.champ.ecommerce.network.beans.Rankings
import com.champ.ecommerce.utils.ModelPreferencesManager
import com.google.gson.Gson

class RankingViewModel : ViewModel() {

    private lateinit var productCatalogue: ProductCatalogue
    private var rankings: MutableLiveData<List<Rankings>>? = null

    fun getData(): MutableLiveData<List<Rankings>>? {

        if (rankings == null) {
            rankings = MutableLiveData()
            getProducts()
        }
        return rankings
    }

    fun getProducts(): MutableLiveData<List<Rankings>>? {
        val categoriesPref = ModelPreferencesManager.get<String>("productData")
        categoriesPref?.let {
                data ->
            val gson = Gson()
            productCatalogue = (gson.fromJson(data, ProductCatalogue::class.java))
            if (productCatalogue.categories.isNotEmpty())
                rankings?.value = productCatalogue.rankings
            return rankings
        }
        return rankings
    }
}